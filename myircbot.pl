#!/usr/bin/perl -W
# Trying to optimize my bot :p
# by Sa[i]nT

use strict;
use IO::Socket;
use LWP::Simple;
use feature qw { switch };

# info block ----------------------------
my $server = "irc.the-beach.co";
my $port = 6667;
my $botnick = "SENTINEL";
my $realname = $botnick . rand(10);
my $nickline = "NICK " . $botnick . "\r\n";
my $userline = "USER " . $realname . " 000 8 * :" . "DIA" . "\r\n";
my @channels = ( "#testing" ,"#home");
# ---------------------------------------

# ---------------------- PIRATE BAY ADD ON
sub getSearchTerm # Search Term
{
    my $searchTerm = shift;
    $searchTerm =~ s/ /%20/g;
    my $newUrl = "http://thepiratebay.se/search/" . $searchTerm . "/0/7/0";
    my $cont = get($newUrl);
    my @results = ();
#    print $cont . "\r\n";
    
    if ( $cont =~ m{Search results: (.+)(?=</span>)</span>&nbsp;(.+)(?=</h2>)} )
    {
        my $searchTerm = $1;
        my $searchResult = $2;
        if ( $searchResult eq "No hits. Try adding an asterisk in you search phrase." )
        {
            return ();
        } else {
            my $acc = 0;
            while ( $cont =~ m{<a href="/torrent/(.+)(?=" class)" class="detLink" title="(.+)(?=">)">(.+)(?=</a>)}g )
            {
#               print $1, $2, $3, "\n";
                push (@results, ($1,$2,$3));
            }
            return @results;
        }
    }
}

# need to just get first 2
sub getFirst2
{
    my $sterm = shift;
    my @res = getSearchTerm($sterm);
    # URL
    # DETAILS
    # TITLE
    my @firstResult = ( $res[0], $res[1], $res[2] );
    my @secondResult = ( $res[3], $res[4], $res[5] );
    my $f_url = "http://thepiratebay.se/" . $res[0];
    my $s_url = "http://thepiratebay.se/" . $res[3];
    return "First Result for $sterm:\0034 $firstResult[2] \003@ (\0033 $f_url \003) | Second Result:\0034 $secondResult[2] \003@ (\0033 $s_url \003)";
}
# ------------




my $sock = IO::Socket::INET->new(
    PeerHost => $server,
    PeerPort => $port,
    Proto    => 'tcp',
) or die "Cannot create socket!\r\n";

sub Out
{
    my $message = shift;
    $message = $message . "\r\n";
    print $sock $message;
}

sub DebugOut
{
    print shift . "\r\n";
}

sub filter_numeric
{
    my $numeric = shift;
    my $message = shift;
    given ($numeric)
    {
		when (m{001})
		{
			# Proper place to send join
			foreach (@channels)
			{
				Out("JOIN " . $_);
			}
			continue;
		}
	}
}

# The bot sends the initial NICK/USER lines to the server.
Out($nickline);
Out($userline);
# -----------

while (<$sock>)
{
    my @lines = split(/\\n/, $_);
    foreach (@lines)
    {
	# bleed it from \r or \n corruption
	$_ =~ s/\R//g;
        given ($_)
        {
            when (length($_) > 0)
            {
#                print $_;
                continue;
            }
            # PING/PONG
            when (m{^PING :(.+)})
            {
                my $c = $1;
                Out("PONG :" . $c);
                DebugOut("PING EVENT");
                continue;
            }
            # NUMERIC CODES
            when (m{^:([^ ]+) ([0-9]+) (.+)}) # get numeric codes
            {
                filter_numeric($2,$3);
                DebugOut("NUMERIC CODE: " . $2 . " -- Message: " . $3);
                continue;
            }
            # PRIVMSG EVENTS FROM CHANNEL/USER
	    when (m{^:(.+)(?=!)!(.+)(?=@)@(.+)(?= ) PRIVMSG ([^ ]+) :(.+)})
	    {
		my $sendingNick = $1;
		my $sendingRealName = $2;
		my $sendingHostName = $3;
		my $channelOrNick = $4;
		my $messageOrCommand = $5;
		DebugOut("PROCESSED MESSAGE: " . $messageOrCommand . " FROM: " . $sendingNick . "");
		when ($messageOrCommand =~ m{^&torrent (.+)})
		{
		    my $tmp = $1;
		    $tmp =~ s/\R//g;
		    Out("PRIVMSG $channelOrNick :" . getFirst2($tmp))
		}
		continue;
	    }
        }
    }
}










